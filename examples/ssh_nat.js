let simple_proxy = require("../simple_proxy");

let local_address = "0.0.0.0";

let dev_1_proxy = new simple_proxy("dev_1", local_address, 2222, "192.168.0.10", 22);
let dev_2_proxy = new simple_proxy("dev_2", local_address, 2223, "192.168.0.11", 22);
let dev_3_proxy = new simple_proxy("dev_3", local_address, 2224, "192.168.0.12", 22);
let dev_4_proxy = new simple_proxy("dev_4", local_address, 2225, "192.168.0.13", 22);