let proxy = require("./proxy");

let auto_connect = false;
let data_exchange_log_enabled = false;
let remoteServer_index = 0;

get_client_by_id = (id) =>
{
    let _ret = null;

    try
    {
        for(let _client_index = 0; _client_index < proxy.proxy_clients.length; _client_index++)
        {
            let _client = proxy.proxy_clients[_client_index];
    
            if(_client.id == id)
            {
                _ret = _client;

                break;
            }
        }
    }
    catch(error)
    {
        throw error;
    }

    return _ret;
}

proxyServer_started = (address, port) =>
{
    try
    {
        logger.info("Proxy server started on " + address + ":" + port);
    }
    catch(error)
    {
        throw error;
    }
}

externalClient_connected = (socket) =>
{
    try
    {
        logger.info("New external client connected");

        if(auto_connect == false)
        {
            // Handle remote server connection

            switch(remoteServer_index)
            {
                case 0:

                    proxy.remoteServer_set_params(remote_server_address, remote_server_port);

                    remoteServer_index++;

                    break;

                case 1:

                    proxy.remoteServer_set_params(remote_server_address_2, remote_server_port_2);

                    remoteServer_index = 0;

                    break;
            }

            proxy.remoteServer_open(socket);
        }
    }
    catch(error)
    {
        throw error;
    }
}

externalClient_disconnected = (socket) =>
{
    try
    {
        logger.info("External client disconnected");
    }
    catch(error)
    {
        throw error;
    }
}

externalClient_tunneled = (id) =>
{
    try
    {
        let _client = get_client_by_id(id);

        logger.info("External client '" + id + "' tunneled to server " + _client.remoteServer_address + ":" + _client.remoteServer_port);

        logger.info("Connected clients count: " + proxy.proxy_clients.length);
    }
    catch(error)
    {
        throw error;
    }
}

externalClient_untunneled = () =>
{
    try
    {
        logger.info("External client untunneled");
    }
    catch(error)
    {
        throw error;
    }
}

remoteServer_data_sent = (id, proxySocket, data) =>
{
    try
    {
        if(data_exchange_log_enabled == false)
        {
            return;
        }

        let _client = get_client_by_id(id);

        let _tx_bytes_count = _client.tx_bytes_count;
        let _rx_bytes_count = _client.rx_bytes_count;

        let _tx_short_count = manage_transfer_results(_tx_bytes_count).short_count;
        let _rx_short_count = manage_transfer_results(_rx_bytes_count).short_count;
        
        let _tx_short_label = manage_transfer_results(_tx_bytes_count).short_label;
        let _rx_short_label = manage_transfer_results(_rx_bytes_count).short_label;

        logger.info("Client " + _client.id + " statistics:");
        logger.info("   Tx bytes count => " + _tx_bytes_count + " [" + _tx_short_count + " " +_tx_short_label + "]");
        logger.info("   Rx bytes count => " + _rx_bytes_count + " [" + _rx_short_count + " " +_rx_short_label + "]");
    }
    catch(error)
    {
        throw error;
    }
}

remoteServer_data_received = (id, proxySocket, data) =>
{
    try
    {
        if(data_exchange_log_enabled == false)
        {
            return;
        }

        let _client = get_client_by_id(id);

        let _tx_bytes_count = _client.tx_bytes_count;
        let _rx_bytes_count = _client.rx_bytes_count;

        let _tx_short_count = manage_transfer_results(_tx_bytes_count).short_count;
        let _rx_short_count = manage_transfer_results(_rx_bytes_count).short_count;
        
        let _tx_short_label = manage_transfer_results(_tx_bytes_count).short_label;
        let _rx_short_label = manage_transfer_results(_rx_bytes_count).short_label;

        logger.info("Client " + _client.id + " statistics:");
        logger.info("   Tx bytes count => " + _tx_bytes_count + " [" + _tx_short_count + " " +_tx_short_label + "]");
        logger.info("   Rx bytes count => " + _rx_bytes_count + " [" + _rx_short_count + " " +_rx_short_label + "]");
    }
    catch(error)
    {
        throw error;
    }
}

manage_transfer_results = (bytesCount) =>
{
    let _ret = 
    {
        "bytes_count" : 0,
        "short_count" : 0,
        "short_label" : ""
    };

    try
    {
        _ret.bytes_count = bytesCount;

        if(bytesCount >= 0 && bytesCount < 1000)
        {
            // Byte

            _ret.short_label = "byte";

            _ret.short_count = Math.round(bytesCount);
        }
        else if(bytesCount >= 1000 && bytesCount < 1000000)
        {
            // KB

            _ret.short_label = "KB";

            _ret.short_count = Math.round(bytesCount / 1000);
        }
        else
        {
            // MB

            _ret.short_label = "MB";

            _ret.short_count = Math.round(bytesCount / (1000 * 1000));
        }
    }
    catch(error)
    {
        throw error;
    }

    return _ret;
}

let local_proxy_address = "0.0.0.0";
let local_proxy_port = 3306;

// Mysql 1

let remote_server_address = "192.168.0.10";
let remote_server_port = 3307;

// Mysql 2

let remote_server_address_2 = "192.168.0.10";
let remote_server_port_2 = 3308;

proxy = new proxy(
{
    "localProxyAddress" : local_proxy_address,
    "localProxyPort" : local_proxy_port,

    "remoteServerAddress" : remote_server_address,
    "remoteServerPort" : remote_server_port,

    "autoConnect" : auto_connect,

    "logEnabled" : log_enabled,

    "proxyServerStarted" : proxyServer_started,

    "externalClientConnected" : externalClient_connected,
    "externalClientDisconnected" : externalClient_disconnected,

    "externalClientTunneled" : externalClient_tunneled,
    "externalClientUntunneled" : externalClient_untunneled,

    "remoteServerDataSent" : remoteServer_data_sent,
    "remoteServerDataReceived" : remoteServer_data_received
});

proxy.start();

setInterval(() =>
{
    // Nothing to do, only for keep-alive
}, 
2000);

setTimeout(() =>
{
    return;

    logger.info("Server switch!");

    proxy.remoteServer_set_params(remote_server_address_2, remote_server_port_2);
}, 
5000);