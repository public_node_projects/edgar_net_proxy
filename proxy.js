// - Powered by pane e cazzimma since 2024 -

const __credits__ = `
    ______________________________________________________________________________________________________________
        
                                        /
                            __       //
                            -\\~,\\=\\ // 
                        --=_\\=---//= 
                        -_ =/  \\/ //\\/ 
                        _,~/   |_   _|\\,
            __          ,/_/    \\' | '/_
        //\\\\          /       | | |\\_
        /(\\ _\\________/        (,_,)       
        J(\\_/                       \\       
        ,)\\/     v                \\   |      
        / Y      (         Y       | /J
    (7 |       \\        |       '/ \\
    '| (       /\\_______\\_     _Y_  \\
        'Y \\     / \\     7   \\   /   \\  \\
        ',) (   /   )   /    |  /    (  )
        '~(  )   / __/     i J     / /     Powered by: 
            \\  /   \\ \\       | |   _/ /      pane e cazzimma since 2024
            | |     )_\\_     )_\\_/__\\/       
            /_\\     |___\\    |___\\
        (___)
    
    "Il vigore di un Jedi scaturisce dalla Forza, ma attento al Lato Oscuro! 
    Rabbia, paura, violenza: sono loro il Lato Oscuro! Veloci ti raggiungono quando combatti! 
    Se anche una sola volta la strada buia tu prendi, per sempre essa dominerà il tuo destino!" - M. Yoda
    
    [Con la speranza di avere sempre il coraggio di non cedere al lato oscuro. - Andrea]
    ______________________________________________________________________________________________________________
`;

let Proxy = class Proxy
{
    /*
    RED CLIENT <==> | EDGAR PROXY | <==> BLUE SERVER
    */

    // Properties

    version = "1.0.3.0";

    auto_data_exchange = true;

    local_proxy_port;
    local_proxy_address = "0.0.0.0";

    remote_server_address;
    remote_server_port;

    proxy_server;
    proxy_clients = [];

    // Handlers

    on_externalClient_connected;
    on_externalClient_disconnected;

    on_externalClient_tunneled;
    on_externalClient_untunneled;

    on_remoteServer_data_sent;
    on_remoteServer_data_received;

    // Constructor

    /*
    Options =>
    {
        localProxyAddress,
        localProxyPort,

        remoteServerAddress,
        remoteServerPort,

        autoConnect,

        autoDataExchange,

        proxyServerStarted,

        externalClientConnected,
        externalClientDisconnected,

        externalClientTunneled,
        externalClientUntunneled,

        remoteServerDataSent,
        remoteServerDataReceived

        externalClientDataSent,
        externalClientDataReceived
    }
    */

    constructor(options)
    {
        try
        {
            // Local proxy data

            this.local_proxy_address = options.localProxyAddress ?? "0.0.0.0";
            this.local_proxy_port = options.localProxyPort ?? 3307;

            // Remote server data

            this.remote_server_address = options.remoteServerAddress ?? "127.0.0.1";
            this.remote_server_port = options.remoteServerPort ?? 3306;

            // Connection

            this.auto_connect = options.autoConnect ?? true;

            // Data exchange

            this.auto_data_exchange = options.autoDataExchange ?? true;

            // Handlers

            this.on_proxyServer_started = options.proxyServerStarted;

            this.on_externalClient_connected = options.externalClientConnected;
            this.on_externalClient_disconnected = options.externalClientDisconnected;

            this.on_externalClient_tunneled = options.externalClientTunneled;
            this.on_externalClient_untunneled = options.externalClientUntunneled;

            this.on_remoteServer_data_sent = options.remoteServerDataSent;
            this.on_remoteServer_data_received = options.remoteServerDataReceived;

            this.on_externalClient_data_sent = options.externalClientDataSent;
            this.on_externalClient_data_received = options.externalClientDataReceived;

            // General

            this.proxy_server = require("./proxy_server");

            // Create new proxyServer instance

            this.proxy_server = new this.proxy_server(
            {
                "address" : this.local_proxy_address, 
                "port" : this.local_proxy_port, 
                "proxyServerStarted" : this.proxyServer_started,
                "externalClientConnected" : this.externalClient_connected,
                "externalClientDisconnected" : this.externalClient_disconnected,
                "dataSent" : this.externalClient_data_sent,
                "dataReceived" : this.externalClient_data_received
            });
        }
        catch(error)
        {
            throw error;
        }
    }

    // Methods

    start = () =>
    {
        try
        {
            // Start proxyServer

            this.proxy_server.start();
        }
        catch(error)
        {
            throw error;
        }
    }
    
    stop = () =>
    {
        try
        {
        }
        catch(error)
        {
            throw error;
        }
    }

    remoteServer_set_params = (address, port) =>
    {
        try
        {
            this.remote_server_address = address;
            this.remote_server_port = port;
        }
        catch(error)
        {
            throw error;
        }
    }

    remoteServer_open = (socket) =>
    {
        try
        {
            // Generate client id

            let _proxy_client_id = (this.proxy_clients.length + 1).toString();

            // Create new remoteServer_client object

            let _proxy_client = new (require("./proxy_client"))(
            {
                "id" : _proxy_client_id,

                "proxySocket" : socket,

                "remoteServerAddress" : this.remote_server_address, 
                "remoteServerPort" : this.remote_server_port, 

                "connected" : this.remoteServer_connected,
                "disconnected" : this.remoteServer_disconnected,

                "dataReceived" : this.remoteServer_data_received,
                "dataSent" : this.remoteServer_data_sent
            });

            // Connect proxyClient to remoteServer

            _proxy_client.connect();

            // Add proxyClient to clients list

            this.proxy_clients.push(_proxy_client);

            // Raise externalClient_tunneled event

            this.raiseEvent_externalClient_tunneled(_proxy_client_id);
        }
        catch(error)
        {
            throw error;
        }
    }

    remoteServer_close = (id) =>
    {
        try
        {
            let _proxy_client = this.get_proxyClient_by_id(id);

            if(_proxy_client == null)
            {
                throw new Error("Proxy client " + id + " not found!");
            }

            _proxy_client.close();
            _proxy_client.proxy_socket.destroy();

            this.remove_proxyClient(id);
        }
        catch(error)
        {
            throw error;
        }
    }

    remoteServer_switch = (address, port) =>
    {
        try
        {
            this.remoteServer_set_params(address, port);

            this.proxy_server.closeAndClear_connected_sockets();
        }
        catch(error)
        {
            throw error;
        }
    }

    get_proxyClient_by_id = (id) =>
    {
        let _ret = null;

        try
        {
            let _proxyClient_index = this.get_proxyClient_index_by_id(id);

            if(_proxyClient_index == -1)
            {
                return _ret;
            }

            _ret = this.proxy_clients[_proxyClient_index];
        }
        catch(error)
        {
            throw error;
        }

        return _ret;
    }

    get_proxyClient_index_by_id = (id) =>
    {
        let _ret = -1;

        try
        {
            for(let _client_index = 0; _client_index < this.proxy_clients.length; _client_index++)
            {
                let _client = this.proxy_clients[_client_index];
        
                if(_client.id == id)
                {
                    _ret = _client_index;

                    break;
                }
            }
        }
        catch(error)
        {
            throw error;
        }

        return _ret;
    }

    get_proxyClient_by_socket = (socket) =>
    {
        let _ret = null;

        try
        {
            let _proxyClient_index = this.get_proxyClient_index_by_socket(socket);

            if(_proxyClient_index == -1)
            {
                return _ret;
            }

            _ret = this.proxy_clients[_proxyClient_index];
        }
        catch(error)
        {
            throw error;
        }

        return _ret;
    }

    get_proxyClient_index_by_socket = (socket) =>
    {
        let _ret = -1;

        try
        {
            for(let _client_index = 0; _client_index < this.proxy_clients.length; _client_index++)
            {
                let _client = this.proxy_clients[_client_index];
        
                if(_client.proxy_socket == socket)
                {
                    _ret = _client_index;

                    break;
                }
            }
        }
        catch(error)
        {
            throw error;
        }

        return _ret;
    }

    remove_proxyClient = (id) =>
    {
        try
        {
            let _proxyClient_index = this.get_proxyClient_index_by_id(id);

            if(_proxyClient_index == -1)
            {
                throw new Error("Proxy client " + id + " not found!");
            }

            this.proxy_clients[_proxyClient_index] = null;

            this.proxy_clients.splice(_proxyClient_index, 1);

            // Raise externalClient_untunneled event

            this.raiseEvent_externalClient_untunneled(id);
        }
        catch(error)
        {
            throw error;
        }
    }

    // Handlers

    // ==> ProxyServer

    proxyServer_started = (address, port) =>
    {
        try
        {
            // ProxyServer started

            // Raise proxyServer_started event

            this.raiseEvent_proxyServer_started(address, port);
        }
        catch(error)
        {
            throw error;
        }
    }

    // ==> ExternalClient

    externalClient_connected = (socket) =>
    {
        try
        {
            // New external client connected

            // Raise externalClient_connected event

            this.raiseEvent_externalClient_connected(socket);

            if(this.auto_connect == true)
            {
                // Remote server open

                this.remoteServer_open(socket);
            }
            else
            {
                // Connection to remote server handled externally
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_disconnected = (socket) =>
    {
        try
        {
            let _proxy_client = this.get_proxyClient_by_socket(socket);

            if(_proxy_client == null)
            {
                return;
            }

            // Close remoteServer client

            this.remoteServer_close(_proxy_client.id);

            // Raise externalClient_disconnected event

            this.raiseEvent_externalClient_disconnected(socket);
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_tunneled = (socket) =>
    {
        try
        {
            // External client tunneled
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_untunneled = (socket) =>
    {
        try
        {
            // External client untunneled
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_data_sent = (socket) =>
    {
        try
        {
            // || Client <= Proxy ||

            // ExternalClient data sent handler

            let _proxy_client = this.get_proxyClient_by_socket(socket);

            if(_proxy_client == null)
            {
                throw new Error("Proxy client bySocket not found!");
            }
        
            // Raise externalClient_data_sent event

            this.raiseEvent_externalClient_data_sent(_proxy_client, socket, data);
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_data_received = (socket, data) =>
    {
        try
        {
            // || Client => Proxy ||

            // ExternalClient data received handler

            let _proxy_client = this.get_proxyClient_by_socket(socket);

            if(_proxy_client == null)
            {
                throw new Error("Proxy client bySocket not found!");
            }

            if(this.auto_data_exchange == true)
            {
                // Send data received from externalClient to remoteServer

                _proxy_client.write(data);
            }

            // Raise externalClient_data_received event

            this.raiseEvent_externalClient_data_received(_proxy_client, socket, data);
        }
        catch(error)
        {
            throw error;
        }
    }

    // ==> RemoteServer

    remoteServer_connected = (id) =>
    {
        try
        {
        }
        catch(error)
        {
            throw error;
        }
    }

    remoteServer_disconnected = (id) =>
    {
        try
        {
        }
        catch(error)
        {
            throw error;
        }
    }

    remoteServer_data_sent = (id, proxySocket, data) =>
    {
        try
        {
            // || Proxy => Server ||

            // Raise remoteServer_data_sent event

            this.raiseEvent_remoteServer_data_sent(id, proxySocket, data);
        }
        catch(error)
        {
            throw error;
        }
    }

    remoteServer_data_received = (id, proxySocket, data) =>
    {
        try
        {
            // || Proxy <= Server ||

            if(this.auto_data_exchange == true)
            {
                // Send data received from remoteServer to externalClient

                proxySocket.write(data);
            }

            // Raise remoteServer_data_received event

            this.raiseEvent_remoteServer_data_received(id, proxySocket, data);
        }
        catch(error)
        {
            throw error;
        }
    }

    // Events

    // ==> ProxyServer

    raiseEvent_proxyServer_started = (address, port) =>
    {
        try
        {
            // Raise on_proxyServer_started event

            if(this.on_proxyServer_started != undefined)
            {
                this.on_proxyServer_started(address, port);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    // ==> ExternalClient

    raiseEvent_externalClient_connected = (id) =>
    {
        try
        {
            // Raise on_externalClient_connected event

            if(this.on_externalClient_connected != undefined)
            {
                this.on_externalClient_connected(id);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_externalClient_disconnected = (id) =>
    {
        try
        {
            // Raise on_externalClient_disconnected event

            if(this.on_externalClient_disconnected != undefined)
            {
                this.on_externalClient_disconnected(id);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_externalClient_tunneled = (id) =>
    {
        try
        {
            // Raise on_externalClient_tunneled event

            if(this.on_externalClient_tunneled != undefined)
            {
                this.on_externalClient_tunneled(id);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_externalClient_untunneled = (id) =>
    {
        try
        {
            // Raise on_externalClient_untunneled event

            if(this.on_externalClient_untunneled != undefined)
            {
                this.on_externalClient_untunneled(id);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    // ==> RemoteServer
    
    // ==> Data_Transfer

    raiseEvent_remoteServer_data_sent = (id, proxySocket, data) =>
    {
        try
        {
            // Raise on_remoteServer_data_sent event

            if(this.on_remoteServer_data_sent != undefined)
            {
                this.on_remoteServer_data_sent(id, proxySocket, data);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_remoteServer_data_received = (id, proxySocket, data) =>
    {
        try
        {
            // Raise on_remoteServer_data_received event

            if(this.on_remoteServer_data_received != undefined)
            {
                this.on_remoteServer_data_received(id, proxySocket, data);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_externalClient_data_sent = (proxyClient, proxySocket, data) =>
    {
        try
        {
            // Raise on_externalClient_data_sent event

            if(this.on_externalClient_data_sent != undefined)
            {
                this.on_externalClient_data_sent(proxyClient, proxySocket, data);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_externalClient_data_received = (proxyClient, proxySocket, data) =>
    {
        try
        {
            // Raise on_externalClient_data_received event

            if(this.on_externalClient_data_received != undefined)
            {
                this.on_externalClient_data_received(proxyClient, proxySocket, data);
            }
        }
        catch(error)
        {
            throw error;
        }
    }
}

module.exports = Proxy;