![Logo](images/edgar_1.png)

---

<h3 align="center">
  <code>edgar-net-proxy</code>
</h3>

## About

A TCP proxy built with NodeJs and a lot of love

## Main options

`localProxyAddress`: Local proxy server address</br>

`localProxyPort`: Local proxy server port</br>

`remoteServerAddress`: Remote server address</br>

`remoteServerPort`: Remote server port</br>

`autoConnect`: Specify if the tunnel to remote server is created automatically or not ('autoConnect = false' necessary for load balancing mode!)</br>

`externalClientConnected`: External client connected (callback)</br>

`externalClientDisconnected`: External client disconnected (callback)</br>

`externalClientTunneled`: External client tunneled to server (callback)</br>

`externalClientUntunneled`: External client untunneled from the server (callback)</br>

`remoteServerDataSent`: Data sent to remote server (callback)</br>

`remoteServerDataReceived`: Data received from remote server (callback)</br>

## Examples

Simple TCP tunnel

```javascript
let simple_proxy = require("./simple_proxy");

let proxy_name = "dev_1";
let local_address = "0.0.0.0";
let local_port = 2222;
let remote_address = "192.168.0.10";
let remote_port = 22;

new simple_proxy(proxy_name, local_address, local_port, remote_address, remote_port).start();
```

Load balancing

```javascript
let proxy = require("./proxy");

let auto_connect = false;
let remoteServer_index = 0;

externalClient_connected = (socket) =>
{
    try
    {
        logger.info("New external client connected");

        if(auto_connect == false)
        {
            // Handle remote server connection

            switch(remoteServer_index)
            {
                case 0:

                    proxy.remoteServer_set_params(remote_server_address, remote_server_port);

                    remoteServer_index++;

                    break;

                case 1:

                    proxy.remoteServer_set_params(remote_server_address_2, remote_server_port_2);

                    remoteServer_index = 0;

                    break;
            }

            proxy.remoteServer_open(socket);
        }
    }
    catch(error)
    {
        throw error;
    }
}

externalClient_disconnected = (socket) =>
{
    try
    {
        logger.info("External client disconnected");
    }
    catch(error)
    {
        throw error;
    }
}

externalClient_tunneled = (id) =>
{
    try
    {
        logger.info("External client '" + id + "' tunneled");

        logger.info("Connected clients count: " + proxy.proxy_clients.length);
    }
    catch(error)
    {
        throw error;
    }
}

externalClient_untunneled = () =>
{
    try
    {
        logger.info("External client untunneled");
    }
    catch(error)
    {
        throw error;
    }
}

let local_proxy_address = "0.0.0.0";
let local_proxy_port = 3306;

// Server 1

let remote_server_address = "192.168.0.10";
let remote_server_port = 3307;

// Server 2

let remote_server_address_2 = "192.168.0.10";
let remote_server_port_2 = 3308;

proxy = new proxy(
{
    "localProxyAddress" : local_proxy_address,
    "localProxyPort" : local_proxy_port,

    "remoteServerAddress" : remote_server_address,
    "remoteServerPort" : remote_server_port,

    "autoConnect" : auto_connect,

    "externalClientConnected" : externalClient_connected,
    "externalClientDisconnected" : externalClient_disconnected,

    "externalClientTunneled" : externalClient_tunneled,
    "externalClientUntunneled" : externalClient_untunneled,

    "remoteServerDataSent" : remoteServer_data_sent,
    "remoteServerDataReceived" : remoteServer_data_received
});

proxy.start();

setInterval(() =>
{
    // Nothing to do, only for keep-alive
}, 
2000);
```
