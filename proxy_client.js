let Proxy_Client = class Proxy_Client
{
    // Properties

    id;

    proxy_client; // Client instance

    proxy_socket; // RemoteClient socket

    remoteServer_address = "127.0.0.1";
    remoteServer_port;

    // Handlers

    on_connected;
    on_disconnected;

    on_data_sent;
    on_data_received;

    tx_bytes_count = 0;
    rx_bytes_count = 0;

    connected = false;

    // Constructor

    /*
    Options =>
    {
        id,

        proxySocket,

        remoteServerAddress,
        remoteServerPort,

        connected,
        disconnected,

        dataReceived,
        dataSent
    }
    */

    constructor(options)
    {
        try
        {
            // Id

            this.id = options.id;

            // Proxy server socket

            this.proxy_socket = options.proxySocket;

            // Proxy client data

            this.remoteServer_address = options.remoteServerAddress ?? "0.0.0.0";
            this.remoteServer_port = options.remoteServerPort ?? 3307;

            // Handlers

            this.on_connected = options.connected;
            this.on_disconnected = options.disconnected;

            this.on_data_received = options.dataReceived;
            this.on_data_sent = options.dataSent;

            // General

            this.net = require("net");
        }
        catch(error)
        {
            throw error;
        }
    }

    // Methods

    connect = () =>
    {
        try
        {
            // Create new client instance

            this.proxy_client = new this.net.Socket();

            // Add client handlers

            this.proxy_client.on("data", (data) => 
            {
                // Increase data received counter

                this.rx_bytes_count += data.length;

                // Raise data_received event

                this.raiseEvent_data_received(this.id, this.proxy_socket, data);
            });

            // Connect to remote server

            this.proxy_client.connect(this.remoteServer_port, this.remoteServer_address, () =>
            {
                this.connected = true;

                // Raise connected event

                this.raiseEvent_connected(this.id);
            });
        }
        catch(error)
        {
            throw error;
        }
    }
    
    write = (data) =>
    {
        try
        {
            // Send data to remote server

            this.proxy_client.write(data, () =>
            {
                // Increase data sent counter

                this.tx_bytes_count += data.length;

                // Raise proxyClientDataSent event

                this.raiseEvent_data_sent(this.id, this.proxy_socket, data);
            });
        }
        catch(error)
        {
            throw error;
        }
    }

    close = () =>
    {
        try
        {
            this.connected = false;

            this.proxy_client.destroy();
        }
        catch(error)
        {
            throw error;
        }
    }

    // Events

    raiseEvent_connected = (id) =>
    {
        try
        {
            if(this.on_connected != undefined)
            {
                // Raise on_connected event

                this.on_connected(id);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_disconnected = (id) =>
    {
        try
        {
            if(this.on_disconnected != undefined)
            {
                // Raise on_disconnected event

                this.on_disconnected(id);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_data_sent = (id, proxySocket, data) =>
    {
        try
        {
            if(this.on_data_sent != undefined)
            {
                // Raise on_data_sent event
                
                this.on_data_sent(id, proxySocket, data);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_data_received = (id, proxySocket, data) =>
    {
        try
        {
            if(this.on_data_received != undefined)
            {
                // Raise on_data_received event

                this.on_data_received(id, proxySocket, data);
            }
        }
        catch(error)
        {
            throw error;
        }
    }
}

module.exports = Proxy_Client;