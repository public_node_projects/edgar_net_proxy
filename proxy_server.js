let Proxy_Server = class Proxy_Server
{
    // Properties

    proxy_server; // Server instance

    address = "127.0.0.1";
    port;

    connected_sockets = [];

    // Handlers

    on_proxyServer_started;

    on_externalClient_connected;
    on_externalClient_disconnected;

    on_data_sent;
    on_data_received;

    // Constructor

    /*
    Options =>
    {
        address,
        port,

        proxyServerStarted,

        externalClientConnected,
        externalClientDisconnected

        dataSent,
        dataReceived
    }
    */

    constructor(options)
    {
        try
        {
            // Proxy server data

            this.address = options.address ?? "0.0.0.0";
            this.port = options.port ?? 3307;

            // Handlers

            this.on_proxyServer_started = options.proxyServerStarted;

            this.on_externalClient_connected = options.externalClientConnected;
            this.on_externalClient_disconnected = options.externalClientDisconnected;

            this.on_data_sent = options.dataSent;
            this.on_data_received = options.dataReceived;

            // General

            this.net = require("net");

            // Create new server instance

            this.proxy_server = this.net.createServer((socket) =>
            {
                socket.on("close", () => 
                {
                    // Raise externalClient_disconnected event
    
                    this.raiseEvent_externalClient_disconnected(socket);
                });
            });

            // Add server handlers

            this.add_proxyServer_handlers();
        }
        catch(error)
        {
            throw error;
        }
    }

    // Methods

    start = () =>
    {
        try
        {
            // Start listening

            this.proxy_server.listen(this.port, this.address, (server) =>
            {
                // Raise data_received event

                this.raiseEvent_proxyServer_started(this.address, this.port);
            });
        }
        catch(error)
        {
            throw error;
        }
    }
    
    stop = () =>
    {
        try
        {
            // Close server

            this.closeAndClear_connected_sockets();

            this.proxy_server.stop();
            this.proxy_server.destroy();
        }
        catch(error)
        {
            throw error;
        }
    }

    closeAndClear_connected_sockets = () =>
    {
        try
        {
            for(let _socket_index = 0; _socket_index < this.connected_sockets.length; _socket_index++)
            {
                this.connected_sockets[_socket_index].end();

                this.connected_sockets.slice(_socket_index, 1);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    // Handlers

    add_proxyServer_handlers = () =>
    {
        try
        {
            this.proxy_server.on("connection", (socket) => 
            {         
                this.connected_sockets.push(socket);

                // Data received handler

                socket.on("data", (data) => 
                {
                    // Raise data_received event

                    this.raiseEvent_data_received(socket, data);   
                });

                // Raise externalClient_connected event

                this.raiseEvent_externalClient_connected(socket);
            });
        }
        catch(error)
        {
            throw error;
        }
    }

    // Events

    raiseEvent_proxyServer_started = (address, port) =>
    {
        try
        {
            if(this.on_proxyServer_started != undefined)
            {
                // Raise on_proxyServer_started event

                this.on_proxyServer_started(address, port);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_data_sent = (socket, data) =>
    {
        try
        {
            if(this.on_data_sent != undefined)
            {
                // Raise on_data_sent event
                
                this.on_data_sent(socket, data);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_data_received = (socket, data) =>
    {
        try
        {
            if(this.on_data_received != undefined)
            {
                // Raise on_data_received event

                this.on_data_received(socket, data);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_externalClient_connected = (socket) =>
    {
        try
        {
            if(this.on_externalClient_connected != undefined)
            {
                // Raise on_externalClient_connected event

                this.on_externalClient_connected(socket);
            }
        }
        catch(error)
        {
            throw error;
        }
    }

    raiseEvent_externalClient_disconnected = (socket) =>
    {
        try
        {
            if(this.on_externalClient_disconnected != undefined)
            {
                // Raise on_externalClient_disconnected event

                this.on_externalClient_disconnected(socket);
            }
        }
        catch(error)
        {
            throw error;
        }
    }
}

module.exports = Proxy_Server;