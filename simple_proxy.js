let logger = require('./log');

let Simple_Proxy = class Simple_Proxy
{
    // Constructor

    proxy_name = "";

    proxy;

    local_address;
    local_port;

    remote_address;
    remote_port;

    constructor
    (
        name,
        localAddress, 
        localPort, 
        remoteAddress, 
        remotePort
    )
    {
        try
        {
            this.proxy_name = name;
            
            this.local_address = localAddress;
            this.local_port = localPort;

            this.remote_address = remoteAddress;
            this.remote_port = remotePort;

            this.proxy = require("./proxy");

            this.proxy = new this.proxy(
            {
                "localProxyAddress" : this.local_address,
                "localProxyPort" : this.local_port,
            
                "remoteServerAddress" : this.remote_address,
                "remoteServerPort" : this.remote_port,

                "proxyServerStarted" : this.proxyServer_started,

                "externalClientConnected" : this.externalClient_connected,
                "externalClientDisconnected" : this.externalClient_disconnected,
            
                "externalClientTunneled" : this.externalClient_tunneled,
                "externalClientUntunneled" : this.externalClient_untunneled,
            });
        }
        catch(error)
        {
            throw error;
        }
    }

    // Methods

    start = () =>
    {
        try
        {
            this.proxy.start();
        }
        catch(error)
        {
            throw error;
        }
    }

    // Handlers

    proxyServer_started = (address, port) =>
    {
        try
        {
            logger.info("[" + this.proxy_name + "]: Proxy server started on " + address + ":" + port);
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_connected = (socket) =>
    {
        try
        {
            logger.info("[" + this.proxy_name + "]: New external client connected");
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_disconnected = (socket) =>
    {
        try
        {
            logger.info("[" + this.proxy_name + "]: External client disconnected");
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_tunneled = (id) =>
    {
        try
        {
            let _client = this.proxy.get_proxyClient_by_id(id);

            logger.info("[" + this.proxy_name + "]: External client '" + id + "' tunneled to server " + _client.remoteServer_address + ":" + _client.remoteServer_port);

            logger.info("[" + this.proxy_name + "]: Connected clients count: " + this.proxy.proxy_clients.length);
        }
        catch(error)
        {
            throw error;
        }
    }

    externalClient_untunneled = () =>
    {
        try
        {
            logger.info("[" + this.proxy_name + "]: External client untunneled");
        }
        catch(error)
        {
            throw error;
        }
    }
}

module.exports = Simple_Proxy;
