var net = require("net");

const server = net.createServer();
const client = new net.Socket();

let server_socket;

server.on("connection", socket => 
{
  logger.info("New client arrived");

  server_socket = socket;

  client.connect(3306, "127.0.0.1", function()
  {
    logger.info("Client connected");
  });

  server_socket.on("data", (data) => 
  {
    //logger.info(data);

    client.write(data);
  });
});

client.on("data", function(data) 
{
  server_socket.write(data);
});

server.listen(3307, "127.0.0.1");